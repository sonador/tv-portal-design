CP.ns('CP.Cockpit.Analytics');

CP.Cockpit.Analytics = (function($, CP, window) {

    var DEFAULTS = {
        URLS: {
            USER_SERVICE_TYPES: '/analytics/user/service/types'
        }
    };

    var Analytics = function() {};


    $.extend(Analytics.prototype, {

        init: function() {
            var _this = this;
            _this.setBasicDimentions();
            _this.initTriggerListeners();
        },

        initTriggerListeners: function() {
            $(CP.Cockpit.Analytics).on('cp.analytics.do.pause.service.package', function() {
                ga('send', 'event', {
                    'eventCategory': 'Management of services',
                    'eventAction': 'Pause basic service'
                });
                console.info("Sended event by pause of service");
            });

            $(CP.Cockpit.Analytics).on('cp.analytics.do.pay.with.system', function(event, paySystem) {
                ga('send', 'event', {
                    'eventCategory': 'Financial actions',
                    'eventAction': 'Payment with pay system',
                    'eventLabel': paySystem
                });
                console.info("Sended event by payment with pay system");
            });

            $(CP.Cockpit.Analytics).on('cp.analytics.activate.additional.service', function(event, serviceId) {
                ga('send', 'event', {
                    'eventCategory': 'Management of services',
                    'eventAction': 'Activate additional service',
                    'eventLabel': serviceId
                });
                console.info("Sended event by activate additional service '%s'", serviceId);
            });

            $(CP.Cockpit.Analytics).on('cp.analytics.add.tv.point', function(event, deviceId) {
                ga('send', 'event', {
                    'eventCategory': 'Management of devices',
                    'eventAction': 'Add additional TV point',
                    'eventLabel': deviceId
                });
                console.info("Sended event by add additional TV point '%s'", deviceId);
            });

            $(CP.Cockpit.Analytics).on('cp.analytics.change.package.service', function(event, serviceId, isUpsale) {
                if (isUpsale == true) {
                    ga('send', 'event', {
                        'eventCategory': 'Management of services',
                        'eventAction': 'Change base service with up sale',
                        'eventLabel': serviceId
                    });

                    console.info("Sended event by change base service '%s' with up sale", serviceId);
                } else {
                    ga('send', 'event', {
                        'eventCategory': 'Management of services',
                        'eventAction': 'Change base service with down sale',
                        'eventLabel': serviceId
                    });
                    console.info("Sended event by change base service '%s' with down sale", serviceId);
                }
            });

            $(CP.Cockpit.Analytics).on('cp.analytics.change.contact.info', function(event, contactType) {
                if (contactType.indexOf("AUX_EMAIL")) {
                    ga('send', 'event', {
                        'eventCategory': 'Management of contacts',
                        'eventAction': 'Change AUX_EMAIL'
                    });
                    console.info("Sended event by change contact info with type '%s'", 'AUX_EMAIL');
                };
                if (contactType.indexOf("VOLIA_EMAIL")) {
                    ga('send', 'event', {
                        'eventCategory': 'Management of contacts',
                        'eventAction': 'Change VOLIA_EMAIL'
                    });
                    console.info("Sended event by change contact info with type '%s'", 'VOLIA_EMAIL');
                };
                if (contactType.indexOf("MOBILE_PHONE")) {
                    ga('send', 'event', {
                        'eventCategory': 'Management of contacts',
                        'eventAction': 'Change MOBILE_PHONE'
                    });
                    console.info("Sended event by change contact info with type '%s'", 'MOBILE_PHONE');
                };
                
            });
        },

        setBasicDimentions: function() {
            this.setUserServiceTypes();
        },

        setUserServiceTypes: function() {
            var _this = this;
            // var checkUrl = _this.createTargetUrl(DEFAULTS.URLS.USER_SERVICE_TYPES);
            var checkUrl = _this.getAnalyticsUserServiceTypesUrl();
            _this.doCallRequest(checkUrl).done(function(response) {
                if (response.status) {
                    console.info("Set user types '%s' to GA", response.checkedResult);

                    ga('set', 'dimension2', response.checkedResult);
                    ga('send', 'pageview');
                }
            }).fail(function(jqXhr, textStatus, errorThrown) {
            	ga('send', 'pageview');
            });
        },

        doCallRequest: function(url) {
            var _this = this;
            return $.ajax({
                cache: false,
                method: 'POST',
                url: url,
                data: {
                    _csrf: $('input[name="_csrf"]').val()
                }
            }).fail(function(jqXhr, textStatus, errorThrown) {
                console.error('"Analytics.loadData" >> ' + jqXhr.status + ' (' + errorThrown + ')');
            });
        },

        createTargetUrl: function(actionUrl) {
            var baseUrl = $('input[name="baseTechUrl"]').val();
            return baseUrl + actionUrl;
        },

        //analytics-user-service-types-url
        getAnalyticsUserServiceTypesUrl: function() {
        	var result = null;
        	var techDiagnosticsDialog = $('div[id="techDiagnosticsDialog"]');
        	if (techDiagnosticsDialog.length > 0) {
        		result = $(techDiagnosticsDialog).attr('analytics-user-service-types-url');
        	}
            return result;
        }
    });


    CP.register(Analytics.prototype);

    return Analytics;

})(jQuery, CP, window);