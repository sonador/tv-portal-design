CP.ns('CP.Cockpit.Authentication');

CP.Cockpit.Authentication = (function($, _, CP) {

    var DEFAULTS = {
        SELECTORS: {
            AUTH_FORM: '#authenticationForm',
            AUTH_BUTTON_SUBMIT: '.authButtonLogIn',
            AUTH_KTVINET_BUTTON_SUBMIT: '.ktvinetAuthButtonLogIn',
            CHECK_BOXES_HIDDEN_BLOCK: '.choiceOptionCheckBox',
            INPUTS_EXIST_ACCOUNT: '.existAccount',
            INPUTS_ABSENT_ACCOUNT: '.absentAccount'
        },
        WRONG_FIELD_CLASS: 'inputText_wrong'
    };

    var Authentication = function() {};

    $.extend(Authentication.prototype, {

        init: function() {
            this.initLithiumUrl();
            this.initObjects();
            this.bindEvents();
            this.initAutoLogin();
            this.initCaptchaLogin();
        },
        
        initCaptchaLogin: function(){
        	if($('a').is('#idReloadCaptchaImage')){
        		$('#idReloadCaptchaImage').click();
        	}
        },
        
        initAutoLogin: function(){
        	var _buttonLogin = $('#buttonSubmitLogin');
        	var username = $('input[name="username"]').val();
        	var password = $('input[name="password"]').val();
        	var isParams = _buttonLogin.data('is-params');
        	if (username && password && isParams){
        		_buttonLogin.trigger("click");
        		_buttonLogin.attr('disabled', true);
        		
        	}
        },

        initObjects: function() {
            this.$logginButton = $(DEFAULTS.SELECTORS.AUTH_BUTTON_SUBMIT);
            this.$ktvinetLogginButton = $(DEFAULTS.SELECTORS.AUTH_KTVINET_BUTTON_SUBMIT);
            this.$existLithiumAccount = $('#existLithiumAccount');
            this.$checkboxesHiddenBlock = $(DEFAULTS.SELECTORS.CHECK_BOXES_HIDDEN_BLOCK);
            this.$authorizationForm = $(DEFAULTS.SELECTORS.AUTH_FORM);
            this.$passwordField = $('#authenticationForm').find('#nickName');
            this.$usernameField = $('#authenticationForm').find('#passwordClub');
            this.$failureMessage = $('#authenticationForm').find('.signin-login-failure');
            this._keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        },

        bindEvents: function() {

            var _this = this;
            // _this.$existLithiumAccount.on('change', this.doSwitchRadioCheckBoxes);
            //_this.$existLithiumAccount.prop('checked', true).trigger('change');
            _this.$checkboxesHiddenBlock.on('click', this.doSwitchRadioCheckBoxes);

            _this.$logginButton.on('click', function(e) {
                _this._checkAssignedUser();

            });
            
            _this.$ktvinetLogginButton.on('click', function(e) {
                _this.ktvinetSubmitAutorization(e);

            });

            _this.initPrepareSubmit();

            $('input[id=nickName]').on('focusout', function() {
                var _input = $(this);
                var nickname = _input.val();
                var url = _input.data('check-nickname-exist-url');
                if (!nickname || nickname.length == 0) {
                    return;
                }
                $.ajax({
                    url: url,
                    data: {
                        nickname: nickname
                    }
                }).done(function(response) {
                    if (response.checkStatus == 'FAIL') {
                        for (var key in response.errorsMap) {
                            var err = "<span class=\"formFieldErrorWithoutBorder\" id=\"" + key + "Id\">" + response.errorsMap[key] + "</span>";
                            $("[name^='" + key + "']").after(err);
                        }
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Server failed to process request' + errorThrown);
                });
            });

        },

        initPrepareSubmit: function() {
            var _this = this;
            $('.inputText').on('focus', function() {
                _this.prepareToSubMit();
                $(this).removeClass(DEFAULTS.WRONG_FIELD_CLASS);
            });
        },

        prepareToSubMit: function() {
            $(this).removeClass('inputText_wrong');
            $("span[class='formFieldError']").each(function(index, item) {
                $(this).remove();
            });
            $("span[class='formFieldErrorWithoutBorder']").each(function(index, item) {
                $(this).remove();
            });

            $('#absentLithiumAccountFields').nextAll('br').remove();
            $('#inputForBeforeError').nextAll('br').remove();
        },

        initLithiumUrl: function() {
            //TODO Need to add create cookie in controller
            var lithiumUrl = $.cookie('LITHIUM_URL');
            if (lithiumUrl) {
                $('input[name=success-url]').val(lithiumUrl);
            }
        },

        // Show and hide assign fields with conditions 
        doSwitchRadioCheckBoxes: function(e) {
            var $this = $(e.target);

            $(DEFAULTS.SELECTORS.CHECK_BOXES_HIDDEN_BLOCK).each(function(index, item) {
                var $item = $(item);
                $item.prop('checked', false);
            });

            $('.hiddenFieldBlock').each(function(index, item) {
                var $item = $(item);
                $item.css({
                    'display': 'none'
                });
            });

            $this.prop('checked', true);
            var hiddenBlock = $this.data('hidden-block');
            $('#' + hiddenBlock).css({
                'display': 'block'
            });
        },

        _checkAssignedUser: function() {
            var _this = this;
            var nickname = $('input[id=nickName]').val();
            var passwordClub = $('input[id=passwordClub]').val();
            var url = $('#authButtonLogIn').data('check-assign-url');
            var urlSignUp = $('#signUpLink').attr('href');

            $.ajax({
                url: url,
                data: {
                    nickname: nickname,
                    passwordClub: passwordClub
                }
            }).done(function(response) {
                if (response.checkStatus == 'ASSIGNED') {
                    _this.doSubmitForm(response.userName, response.password);

                } else if (response.checkStatus == 'NICKNAME_EXIST') {
                    _this.initConfirmFragment();
                } else if (response.checkStatus == 'NOT_ASSIGNED') {
                    location.href = urlSignUp;
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert('Server failed to process request' + errorThrown);
            });

        },

        _checkAssignedUserCockpit: function(e, el) {
            e.preventDefault();
            var _this = CP.Cockpit.Authentication.prototype;
            var _el = $(el);
            var url = _el.data('check-account-exist-url');
            var lithiumUrl = _el.attr('href');
            var urlAssignFragm = _el.data('assign-fragm');

            $.ajax({
                url: url
            }).done(function(response) {
                if (response.checkStatus == 'NICKNAME_EXIST') {
                    location.href = lithiumUrl;

                } else if (response.checkStatus == 'NOT_ASSIGNED') {
                    _this.renderCockpitFragment(urlAssignFragm).done(function(html) {
                        $('.onPagePopup .onPagePopupContainer').html(html);
                        CP.Cockpit.showDialogInFancyBox("#onPagePopupId");
                        $('#hrefAbsentAssignClubAccount').attr('data-nick-name', response.userName);
                    });
                } else if (response.checkStatus == 'FAIL') {
                    return; //TODO
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert('Server failed to process request' + errorThrown);
            });
        },
        
        renderCockpitContactAssignClubAccountFragment: function(e, el) {
            e.preventDefault();
            var _this = CP.Cockpit.Authentication.prototype;
            var urlAssignFragm = $(el).data('fragment-url');
            _this.renderCockpitFragment(urlAssignFragm).done(function(html) {
                $('.onPagePopup .onPagePopupContainer').html(html);
                CP.Cockpit.showDialogInFancyBox("#onPagePopupId");
               });
        },

        renderCockpitAssignClubAccountFragment: function(e, el) {
            e.preventDefault();
            var _this = CP.Cockpit.Authentication.prototype;
            var urlAssignFragm = $(el).data('fragment-url');
            var nickName = $(el).data('nick-name');
            _this.renderCockpitFragment(urlAssignFragm).done(function(html) {
                $('.onPagePopup .onPagePopupContainer').html(html);
                CP.Cockpit.showDialogInFancyBox("#onPagePopupId");
                scrolledPriceHolderBodyInfoSwicth();
                _this.initPrepareSubmit();
            });
        },

        renderCockpitFragment: function(urlAssignFragm) {
            return $.ajax({
                url: urlAssignFragm,
                cache: false
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert('Server failed to process request' + errorThrown);
            });
        },

        initConfirmFragment: function() {
            $('#additionFieldContainer').css({
                'display': 'block'
            });
            $('#authButtonLogIn').css({
                'display': 'none'
            });
            $('#confirmButtonLogIn').css({
                'display': 'block'
            });
            ('click', $.proxy(this.cancellAction, this));
            $('#confirmButtonLogIn').on('click', $.proxy(function(e) {
                var assigned = $(this).data('assigned-account');
                if (!assigned) {
                    this.doAssignOrCreate(e);
                }
            }, this));
        },

        getCheckedAssignOption: function() {
            var _this = this;
            var name = null;
            _this.$checkboxesHiddenBlock.each(function() {
                var checked = $(this).prop('checked');
                if (checked) {
                    name = $(this).attr("name");
                }

            });
            return name;
        },

        doAssignOrCreate: function(e) {
            e.preventDefault();
            var _this = this;
            var ckeckedOption = this.getCheckedAssignOption();
            var userType = _this.$logginButton.data('user-type');
            if (ckeckedOption == null) {
                e.preventDefault();
            } else if (ckeckedOption == 'existLithiumAccount') {
                _this.doAssignLithiumAccount();
            } else if (ckeckedOption == 'absentLithiumAccount') {
                _this.doExternalAssignLithiumAccount();
            }
        },

        doAssignLithiumAccount: function() {

            var _this = this;
            var url = $('#existLithiumAccountFields').data('assign-custom-url');

            if (_this.validateBeforeSubmit($(DEFAULTS.SELECTORS.INPUTS_EXIST_ACCOUNT))) {
                var contractCode = $('input[name=contractCodeCustomer]').val();
                var nickname = $('input[name=nickName]').val();
                var email = $('input[name=voliaEmailCustomer]').val();
                var passwordClub = $('input[name=passwordClub]').val();
                var password = $('input[name=accountPassword]').val();

                $.ajax({
                    url: url,
                    data: {
                        contractCode: contractCode,
                        nickname: nickname,
                        email: email,
                        passwordClub: passwordClub,
                        password: password
                    },
                    beforeSend: function() {
                        _this.prepareToSubMit();
                    }
                }).done(function(response) {
                    if (response.checkStatus == 'ASSIGNED') {
                        _this.doSubmitForm(response.userName, response.password);
                    } else {
                        for (var key in response.errorsMap) {
                            var err = "<span class=\"formFieldErrorWithoutBorder\">" + response.errorsMap[key] + "</span><br/>";
                            $('#absentLithiumAccountFields').after(err);

                            $.each(key.split(','), function(index, value) {
                                $('input[name=' + value + ']').addClass(DEFAULTS.WRONG_FIELD_CLASS);
                            });
                        }
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Server failed to process request' + errorThrown);
                });

            }
        },

        doCockpitAssignLithiumAccount: function(e, el) {
            e.preventDefault();
            var _this = this;
            var url = $(el).data('assign-account-url');
            var lithiumUrl = $(el).data('lithium-url');

            if (_this.validateBeforeSubmit($(DEFAULTS.SELECTORS.INPUTS_EXIST_ACCOUNT))) {
                var nickname = $('input[name=nickName]').val();
                var email = $('input[name=voliaEmailCustomer]').val();
                $.ajax({
                    url: url,
                    data: {
                        nickname: nickname,
                        email: email
                    },
                    beforeSend: function() {
                        _this.prepareToSubMit();
                    }
                }).done(function(response) {
                    if (response.checkStatus == 'NICKNAME_EXIST') {
                        location.href = lithiumUrl;
                    } else {
                        for (var key in response.errorsMap) {
                            var err = "<span class=\"formFieldErrorWithoutBorder\">" + response.errorsMap[key] + "</span><br/>";
                            $('#inputForBeforeError').after(err);

                            $.each(key.split(','), function(index, value) {
                                $('input[name=' + value + ']').addClass(DEFAULTS.WRONG_FIELD_CLASS);
                            });

                        }

                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Server failed to process request' + errorThrown);
                });

            }
        },

        doExternalAssignLithiumAccount: function() {

            var _this = this;
            var url = $('#absentLithiumAccountFields').data('assign-custom-url');

            if (_this.validateBeforeSubmit($(DEFAULTS.SELECTORS.INPUTS_ABSENT_ACCOUNT))) {

                var nickname = $('input[name=nickName]').val();
                var email = $('input[name=voliaEmailNonCustomer]').val();
                var passwordClub = $('input[name=passwordClub]').val();
                var confirmPasswordClub = $('input[name=confirmPasswordClub]').val();

                $.ajax({
                    url: url,
                    data: {
                        nickname: nickname,
                        email: email,
                        passwordClub: passwordClub,
                        confirmPasswordClub: confirmPasswordClub
                    },
                    beforeSend: function() {
                        _this.prepareToSubMit();
                    }
                }).done(function(response) {
                    if (response.checkStatus == 'ASSIGNED') {
                        _this.doSubmitForm(response.userName, response.password);
                    } else {
                        for (var key in response.errorsMap) {
                            var err = "<span class=\"formFieldErrorWithoutBorder\">" + response.errorsMap[key] + "</span><br/>";
                            $('#absentLithiumAccountFields').after(err);

                            $.each(key.split(','), function(index, value) {
                                $('input[name=' + value + ']').addClass(DEFAULTS.WRONG_FIELD_CLASS);
                            });

                        }
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Server failed to process request' + errorThrown);
                });

            }
        },

        doCreateInternalUserLithiumAccount: function() {
            var _this = this;

            if (_this.validateBeforeSubmit($(DEFAULTS.SELECTORS.INPUTS_EXIST_ACCOUNT))) {
                var url = $('.signupLoginButton').data('sign-up-url');
                var postData = $('#authenticationForm').serializeArray();
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: postData,
                    beforeSend: function() {
                        _this.prepareToSubMit();
                    }
                }).done(function(response, status, xhr) {
                    if (response.checkStatus == 'ASSIGNED') {
                        _this.doSubmitForm(response.userName, response.password);
                    } else {
                        for (var key in response.errorsMap) {
                            var err = "<span class=\"formFieldErrorWithoutBorder\" id=\"" + key + "Id\">" + response.errorsMap[key] + "</span>";
                            $("[name^='" + key + "']").after(err);
                        }
                    }

                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Server failed to process request' + errorThrown);
                });
            }
        },

        doCreateExternalUserLithiumAccount: function() {
            var _this = this;

            if (_this.validateBeforeSubmit($(DEFAULTS.SELECTORS.INPUTS_ABSENT_ACCOUNT))) {

                var url = $('.signupLoginButton').data('sign-up-url');
                var postData = $('#authenticationForm').serializeArray();

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: postData,
                    beforeSend: function() {
                        _this.prepareToSubMit();
                    }
                }).done(function(response) {
                    if (response.checkStatus == 'ASSIGNED') {
                        _this.doSubmitForm(response.userName, response.password);
                    } else {

                        for (var key in response.errorsMap) {
                            var err = "<span class=\"formFieldErrorWithoutBorder\" id=\"" + key + "Id\">" + response.errorsMap[key] + "</span><br/>";
                            $("[name^='" + key + "']").after(err);
                        }
                    }

                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert('Server failed to process request' + errorThrown);
                });
            }
        },

        doSubmitForm: function(username, password) {
            var _this = this;
            var action = _this.$authorizationForm.attr('action');
            var successUrl = $('input[name=success-url]').val();
            var postData = [];
            postData.push({
                name: "username",
                value: username
            });
            postData.push({
                name: "password",
                value: password
            });
            postData.push({
                name: "success-url",
                value: successUrl
            });
            _this.hideFailureMessage();
            $.ajax({
                type: 'POST',
                url: action,
                data: postData
            }).done(function(res, status, xhr) {
                CP.loggedIn = $.cookie('Volia-Customer-LoggedIn');
                window.location.href = xhr.getResponseHeader("X-Success-Url")
            }).error(function() {
                _this._handleFailure();
            });

        },

        validateBeforeSubmit: function(formInputs) {
            var _this = this;
            var isValid = true;
            formInputs.each(function() {
                _this.validate($(this));
            });
            formInputs.each(function() {
                if ($(this).hasClass('inputText_wrong')) {
                    isValid = false;
                    return false;
                };
            });

            return isValid;
        },

        validate: function($el) {
            if (_.isEmpty($el.val())) {
                $el.addClass('inputText_wrong');
            } else {
                $el.removeClass('inputText_wrong');
            }
        },

        _handleFailure: function() {
            this.$passwordField.addClass(DEFAULTS.WRONG_FIELD_CLASS);
            this.$usernameField.addClass(DEFAULTS.WRONG_FIELD_CLASS);
            this.$failureMessage.show();
        },

        hideFailureMessage: function() {
            this.$passwordField.removeClass(DEFAULTS.WRONG_FIELD_CLASS);
            this.$usernameField.removeClass(DEFAULTS.WRONG_FIELD_CLASS);
            this.$failureMessage.hide();
        },
        
        ktvinetSubmitAutorization: function(e) {
        	e.preventDefault();
        	var username = $('input[name="j_username"]').val();
        	var password = $('input[name="j_password"]').val();
        	var domainUrl = $('input[name="domainUrl"]').val();
        	location = domainUrl + "signin?session=" + this.base64Encode(username + ":" + password)/* + "&password=" + password*/;
        },
        
        base64Encode: function(input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;

            input = this._utf8_encode(input);

            while (i < input.length) {

                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

            }

            return output;
        },
        
        _utf8_encode: function(string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";

            for (var n = 0; n < string.length; n++) {

                var c = string.charCodeAt(n);

                if (c < 128) {
                    utftext += String.fromCharCode(c);
                }
                else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }

            return utftext;
        }

    });

    Authentication.saveLithiumNickName = function(el, event) {
        event.preventDefault();
        var newNickName = $('input[name="lithiumNickName"]').val();
        var oldNickName = $('input[name="oldLithiumNickName"]').val();
        var saveNickNameUrl = $(el).data('save-nickname-url');
        var urlForRedirect = $(el).data('url-for-redirect');
        var _this = CP.Cockpit.Authentication.prototype;
        if (newNickName != null && newNickName.length > 0) {
            $.ajax({
                method: 'POST',
                url: saveNickNameUrl,
                data: {
                    nickname: newNickName
                },
                beforeSend: function() {
                    _this.prepareToSubMit();
                }
            }).done(function(response) {
                if (response.checkStatus == 'NICKNAME_EXIST') {
                    CP.Cockpit.showInfoDialogWithRedirectAfter(Authentication.getMessageChangedNickName, urlForRedirect);
                } else if (response.checkStatus == 'NOT_ASSIGNED'){
                	CP.Cockpit.showInfoDialogWithRedirectAfter(Authentication.getMessageNotExistNickName, null);
                } else {
                	_this.initPrepareSubmit();
                	for (var key in response.errorsMap) {
                	var err = "<span class=\"formFieldErrorWithoutBorder\">" + response.errorsMap[key] + "</span><br/>";
                    $('#inputForBeforeError').after(err);

                    $.each(key.split(','), function(index, value) {
                        $('input[name=' + value + ']').addClass(DEFAULTS.WRONG_FIELD_CLASS);
                    });
                }
                	}

            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert('Server failed to process request' + errorThrown);
            });

        } else {
            CP.Cockpit.showInfoDialogWithRedirectAfter(Authentication.getMessageNotChangedNickName, null);
        }
    };

    Authentication.getMessageNotChangedNickName = function() {
        var message = {
            'UK': 'Помилка змінення Воля ID. Будь ласка, заповніть поле',
            'RU': 'Ошибка изменения Воля ID. Пожалуйста, заполните поле'
        };
        return message[CP.ns('CP.Site.Menu').lang];
    };

    Authentication.getMessageChangedNickName = function() {
        var message = {
            'UK': 'Воля ID успішно змінене',
            'RU': 'Воля ID успешно изменено'
        };
        return message[CP.ns('CP.Site.Menu').lang];
    };

    Authentication.getMessageNotExistNickName = function() {
        var message = {
            'UK': 'Воля ID не існує. Будь ласка, зареєструйтесь',
            'RU': 'Воля ID не существует. Пожалуйста, зарегистрируйтесь'
        };
        return message[CP.ns('CP.Site.Menu').lang];
    };

    CP.register(Authentication.prototype);

    return Authentication;

})(window.jQuery, window._, CP);