CP.ns('CP.Cockpit.Dashboard.Applications');

CP.Cockpit.Dashboard.Applications = (function($, CP) {

    var DEFAULTS = {
        SELECTORS: {
            SCROLLED_HOLDER: '.scrolledPriceHolder',
            SCROLLED_HOLDER_BODY: '.scrolledPriceHolder__body',
            SWITCHER_SHOW_FORM: '.application__create_form__toggle__switcher'
        },

        DATE_FORMAT: 'dd.mm.yy',
        CLASSES: {
            SCROLLED_HOLDER_ACTIVE: 'scrolledPriceHolder_active'
        }
    };

    var Applications = function() {};

    function showDataInModal(html) {
        var mainModal = $('#main-dialog');
        var modalBody = mainModal.find('.modal-content');
        modalBody.html(html);
        mainModal.modal('show');
    }

    $.extend(Applications.prototype, {

        init: function() {
            this.$trafficForm = $('#application__form__traffic');
            this.$fixedIP = $('#application__form__ip');
            this.closeApplicationButton();
            this.bindEvents();
            this.setDatePicker();
            this.bindEventsForDate();
            // Проверка существования задачи для закрытых заявок
            this.checkPresenceTaskForClosedApplications();
            this.loadActiveApplicationList("activeApplications", "applicationsHistory");
        },

        bindEvents: function() {
            $('.application__create_form__toggle__switcher').on('click', $.proxy(this.slideToggleForm, this));
            $('.application__create__cancel_btn').on('click', $.proxy(this.cancellAction, this));

            this.$trafficForm.on('submit', $.proxy(this.createNewTraffic, this));
            this.$fixedIP.on('submit', $.proxy(this.createFixedIP, this));
            this.inputsChangeListener({
                required: '#paymentGuaranteeTraffic, #trafficDatepickerAt, #trafficDatepickerTill',
                toggle: '#createTrafficApplicationBtn'
            });
            this.inputsChangeListener({
                required: '#paymentGuaranteeIp, #macAddress,#privateIpDatepickerAt',
                toggle: '#createFixedIP'
            });

        },

        bindEventsForDate: function() {
            $('input[id="trafficDatepickerAt"]').on('change', $.proxy(this.updatePrice, this));
            $('input[id="trafficDatepickerTill"]').on('change', $.proxy(this.updatePrice, this));
        },

        updatePrice: function() {
            var _this = this;
            var dateFrom = $('input[id=trafficDatepickerAt]').val();
            var dateTill = $('input[id=trafficDatepickerTill]').val();
            var price = $('input[id=priceDetail]').val();

            if (!_this.isEmptyOrNull(dateFrom) || !_this.isEmptyOrNull(dateTill) || !_this.isEmptyOrNull(price)) {
                return;
            }

            $.ajax({
                url: CP.Cockpit.createActionUrl('/application/updated/price/detail/traff'),
                data: {
                    dateFrom: dateFrom,
                    dateTill: dateTill,
                    price: price
                }
            }).done(function(response) {
                $('.packageSpotPriceDetailTraff').html(response);

            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert('Server failed to process request' + errorThrown);
            });
        },

        // Проверка существования задачи для закрытых заявок
        checkPresenceTaskForClosedApplications: function() {
        	var evaluationQualityUrl = null;
        	var container = $("#userClosedApplicationList");
        	if (container != null && container.length > 0) {
        		evaluationQualityUrl = $(container).attr("evaluation-quality-url");
        	}
        	var elements = $("a[class='userRequstList__item__close']");
        	if (elements != null && elements.length > 0) {
            	if (evaluationQualityUrl == null || evaluationQualityUrl.length == 0) {
                	console.error("Error: The parameter \"evaluation-quality-url\" is empty.");
                	return;
            	}
            	var appIds = "";
            	for (i = 0; i < elements.length; i++) {
            		var appId = $(elements[i]).attr("app-id");
            		if (appId != null && appId.length > 0) {
                		appIds = appIds + (i == 0 ? "": ",") + appId;
            		}
            	}
            	var csrf = $("input[name=\"_csrf\"]").val();
            	// Получить признак наличия задачи для заявки.
                $.ajax({
                	method: 'POST',
                    url: evaluationQualityUrl,
                    data: { appIds: appIds, _csrf: csrf }
                }).done(function(response) {
                	response = "," + response + ",";
                	for (i = 0; i < elements.length; i++) {
                		var item = $(elements[i]);
                		var appId = "," + item.attr("app-id") + ",";
                		if (appId != null && appId.length > 2) {
                			// Если по заявке appId имеются задачи 
                    		if (response.indexOf(appId) > -1) {
                    			// то не отображать кнопку проведения анкеты
                    			item.remove(); 
                    			console.info("For appId=" + appId + " already have the task of 'taskCase.code' : 'EvaluationOfTechnique'.");
                    		} else {
                    			// иначе отобразить кнопку проведения анкеты
                    			$(item).show();
                    		}
                		}
                	}
                }).fail(function(jqXHR, textStatus, errorThrown) {
                	console.error("Server failed to process request " + errorThrown);
                });
        	}
        },

        // Определить реакцию на закрытие заявки
        closeApplicationButton: function() {
            $('.closeApplicationButton').click(
                function() {
                    var applicationContainer = $(this);
                    var applicationId = applicationContainer.attr('id');
                    $.ajax({
                        url: applicationContainer.data('application-close-url') + "?applicationId=" + applicationId,
                        cache: false,
                        success: function(html) {
                            showDataInModal(html);
                        }
                    });
                });

        },

        setDatePicker: function() {
            var _this = this;
            $.datepicker.setDefaults($.datepicker.regional["ru"]);

            var $privateIpDatepickerAt = $("#privateIpDatepickerAt");
            var $trafficDatepickerAt = $("#trafficDatepickerAt");
            var $trafficDatepickerTill = $("#trafficDatepickerTill");

            $privateIpDatepickerAt.datepicker({
                showOn: "button",
                minDate: "+1",
                buttonImage: "/cockpit/images/icon/ic_input_calendar.png",
                buttonImageOnly: true,
                dateFormat: DEFAULTS.DATE_FORMAT,
                beforeShowDay: $.datepicker.noWeekends
            });

            $trafficDatepickerAt.datepicker({
                showOn: "button",
                maxDate: "-1",
                buttonImage: "/cockpit/images/icon/ic_input_calendar.png",
                buttonImageOnly: true,
                dateFormat: DEFAULTS.DATE_FORMAT,
                onSelect: function(selectedDate) {
                    $trafficDatepickerTill.datepicker('option', {
                        disabled: false
                    });
                    $trafficDatepickerTill.datepicker("option", "minDate", selectedDate);
                    $(this).change();
                }
            });

            $trafficDatepickerTill.datepicker({
                showOn: "button",
                maxDate: "0",
                buttonImage: "/cockpit/images/icon/ic_input_calendar.png",
                buttonImageOnly: true,
                dateFormat: DEFAULTS.DATE_FORMAT,
                onSelect: function() {
                    $(this).change();
                }
            });
            $trafficDatepickerTill.datepicker('option', {
                disabled: true
            });
        },

        slideToggleForm: function(e) {
            this.closeAllToggleForm(e);
            var $this = $(e.target);
            var $parent = $this.closest(DEFAULTS.SELECTORS.SCROLLED_HOLDER);
            var $hidden = $parent.find(DEFAULTS.SELECTORS.SCROLLED_HOLDER_BODY);
            $hidden.slideToggle(300);
            $parent.toggleClass(DEFAULTS.CLASSES.SCROLLED_HOLDER_ACTIVE);
            $this.off().on('click', $.proxy(this.cancellAction, this));
        },

        cancellAction: function(e) {
            this.hideToggleForm($(e.target));
        },

        hideToggleForm: function($this) {
            var $parent = $this.parents(DEFAULTS.SELECTORS.SCROLLED_HOLDER);
            var $hidden = $parent.find(DEFAULTS.SELECTORS.SCROLLED_HOLDER_BODY);
            $hidden.slideUp(300);
            $parent.removeClass(DEFAULTS.CLASSES.SCROLLED_HOLDER_ACTIVE);
            $this.off().on('click', $.proxy(this.slideToggleForm, this));
        },

        closeAllToggleForm: function(e) {
            var _this = this;
            $('.application__create_form__toggle__switcher').each(function(index, item) {
                _this.hideToggleForm($(item));

            });
        },

        /**
         * Call this function to enable/disable toggle selector option.
         *
         * @param opts - {required: 'usual jquery selectors here', toggle: 'jquery selector for element which will be toggled'}
         */
        inputsChangeListener: function(opts) {
            var options = opts || {};

            if (options.required) {
                var $required = $(options.required);

                var proxy = function tumblerSwitch() {
                    var $toggleSwitch = $(opts.toggle);
                    var result = true;
                    $required.each(function() {
                        var $this = $(this);
                        if ($this.prop('type') === 'checkbox' && !$this.prop('checked')) {
                            result = false;
                        }
                        if ($this.prop('type') === 'text' && !$this.val()) {
                            result = false;
                        }
                    });
                    if (result) {
                        $toggleSwitch.prop('disabled', false);
                    } else {
                        $toggleSwitch.prop('disabled', true);
                    }
                };

                $required.keypress(proxy).keyup(proxy).change(proxy);
            }
        },

        createNewTraffic: function(e) {
            e.preventDefault();
            var _this = this;
            var data = _this.$trafficForm.toObject();
            var url = _this.$trafficForm.data('action');
            CP.Cockpit.Form.SubmitProvider.runAjaxPostRequest(url, data);
        },

        createFixedIP: function(event) {
            event.preventDefault();
            if ($("#useBonuses").val() == "true") {
                CP.Site.FancyBox.showInfoYesNoDialogWithRedirectAfter('prepareActivateAdditional');
                return false;
            } else {
                initConnectFixedIp();
            }
        },

        isEmptyOrNull: function(text) {
            if (text && text.length > 0) {
                return true;
            } else {
                return false;
            }
        },

        loadActiveApplicationList: function(parentElementNameShouldBeActivated, parentElementNameShouldBeDisActivated) {
            var applicationListBlock = $('#activeApplications');
            if (applicationListBlock != null && applicationListBlock != undefined && applicationListBlock.length > 0) {
                var dataObj = {_csrf: $("input[name=\"_csrf\"]").val(), blockState: ""};
                getApplicationListByAjax(parentElementNameShouldBeActivated, parentElementNameShouldBeDisActivated, dataObj);
            }
        }

    });
	
	Applications.cancelApplication = function (el, event) {
		var macRemoveDoUrl = $(el).attr('data-application-cancel-do-url');
		var idToCancel = $(el).attr('data-application-id');
        if (event.preventDefault) {
			event.preventDefault();
		} else { 
			event.returnValue = false; 
		}
        CP.Cockpit.Form.SubmitProvider.runAjaxPostRequest(macRemoveDoUrl, {
            _csrf: $('input[name="_csrf"]').val(),
            appId: idToCancel,
        });
    };

    Applications.loadApplicationFullList = function (parentElementNameShouldBeActivated, parentElementNameShouldBeDisActivated) {
        var dataObj = {_csrf: $("input[name=\"_csrf\"]").val(), blockState: "full"};
        getApplicationListByAjax(parentElementNameShouldBeActivated, parentElementNameShouldBeDisActivated, dataObj);
    };

    function getApplicationListByAjax(parentElementNameShouldBeActivated, parentElementNameShouldBeDisActivated, dataObj) {
        $.ajax({
            url: CP.Cockpit.createActionUrl("/applications/list"),
            cache: false,
            type: "POST",
            data: dataObj,
            beforeSend: function () {
                $('#' + parentElementNameShouldBeDisActivated).hide();
                $('#' + parentElementNameShouldBeActivated).show();
                $('#active_block_spinner_' + parentElementNameShouldBeActivated).show();
            }
        }).success(function (data) {
            $('#active_block_spinner_' + parentElementNameShouldBeActivated).hide();
            $('#applicationListBlock_' + parentElementNameShouldBeActivated).html(data.htmlContent);
        }).fail(function (data) {
            console.error(data);
            $('#active_block_spinner_' + parentElementNameShouldBeActivated).hide();
            $('#errorMessage_' + parentElementNameShouldBeActivated).show();
        });
    };

    Applications.closeApplicationButton = function (_this, event) {
        event.preventDefault();
        var applicationContainer = $(_this);
        var applicationId = applicationContainer.attr('appid');
        var link = applicationContainer.attr('application-close-url');
        var csrf = $("input[name=\"_csrf\"]").val();
        $.ajax({
            data: {_csrf: csrf},
            url: link + "?appId=" + applicationId,
            cache: false
        }).success(function (data) {
            $.fancybox.hideLoading();
            CP.Cockpit.Form.SubmitProvider.ajaxJsonResposeHandler(data, function (html) {
                $("#popupContainerChangeDateExec").html(html);
                $("#scrollbarContainer").scrollbar();
                CP.Cockpit.Dashboard.Applications.popups();
                CP.Cockpit.showDialogInFancyBox("#popupChangeDateExecId");
            });
        }).fail(function (data) {
            $.fancybox.hideLoading();
        });
    };

    Applications.showListChangeDateExecDialog = function (_this, event) {
        event.preventDefault();
        var listChangeDateExecUrl = $(_this).attr("list-change-date-exec-url");
        var dataElem = $(_this).data();
        var dataObj = CP.Cockpit.Dashboard.Applications.simpleCloning($(_this).data());
        if (CP.Cockpit.Dashboard.Applications.checkIsEmptyAndShowMessage(listChangeDateExecUrl, "Server failed to process request")) {
        	console.error("Error: The parameter \"list-change-date-exec-url\" is empty.");
        	return;
        }
        $.ajax({
            url: listChangeDateExecUrl,
            cache: false,
            beforeSend: $.fancybox.showLoading(),
            data: dataObj
        }).done(function (response) {
        	$.fancybox.hideLoading();
            CP.Cockpit.Form.SubmitProvider.ajaxJsonResposeHandler(response, function (html) {
            	// Добавить полученный фрагмент в элемент div
            	$("#popupContainerChangeDateExec").html(html);
                // Установить реакцию на scrollbar.
                $("#scrollbarContainer").scrollbar();
                // Установить реакцию на нажатие кнопки "close".
                CP.Cockpit.Dashboard.Applications.popups();
                // Отобразить popup-окно в диалоговом режиме.
                CP.Cockpit.showDialogInFancyBox("#popupChangeDateExecId");
            });
        }).fail(function (data) {
        	$.fancybox.hideLoading();
            alert("Server failed to process request");
        });
    };

    Applications.showChangeDateExecDialog = function (_this, event) {
        event.preventDefault();
        var actionUrl = $(_this).attr("change-date-exec-url");
        var dataElem = $(_this).data();
        var dataObj = CP.Cockpit.Dashboard.Applications.simpleCloning($(_this).data());
        if (CP.Cockpit.Dashboard.Applications.checkIsEmptyAndShowMessage(actionUrl, "Server failed to process request")) {
        	console.error("Error: The parameter \"change-date-exec-url\" is empty.");
        	return;
        }
        // Выполнить запрос и отобразить результат
        CP.Cockpit.Form.SubmitProvider.runAjaxPostRequest(actionUrl, dataObj);
    };

    Applications.simpleCloning = function (obj){ 
        if(obj == null || typeof(obj) != 'object') 
            return obj; 
        if(obj.constructor == Array) 
            return [].concat(obj); 
        var temp = {}; 
        for(var key in obj) 
            temp[key] = obj[key]; 
        return temp; 
    };
    
    Applications.checkIsEmptyAndShowMessage = function (dataStr, messageText) {
    	var result = dataStr == null || dataStr.length == 0;
		if (result) {
			alert(messageText);
    	}
		return result;
    };

    // Установить реакцию на нажатие кнопки "close".
    Applications.popups = function () {
        $(".onPagePopup__close").on("click", function () {
            var $this = $(this);
            CP.Cockpit.Dashboard.Applications.popupClose();
            return false;
        });
    }
    // Реализация закрытия popup-окна.
    Applications.popupClose = function () {
    	$(".onPagePopup").fadeOut();
    	$(".overlay").fadeOut();
    	$.fancybox.close();
    };

    Applications.connectFixedIp = function () {
        initConnectFixedIp();
    };
    
    function initConnectFixedIp() {
        var _form = $("#application__form__ip");
        var data = $(_form).toObject();
        var url = $(_form).attr('action');
        CP.Cockpit.Form.SubmitProvider.runAjaxPostRequest(url, data);
    };
    
    CP.register(Applications.prototype);

    return Applications;

})(jQuery, CP);