CP.ns('CP.Support');

CP.Support = (function($, _, CP) {
	
    var DEFAULTS = {
        SELECTORS: {
            TECH_BODY_CONTAINER: '#techBodyContainer',
            TECH_BODY_CONTAINER2: '#techBodyContainer2',
            TECH_SUPPORT_DIALOG: '#techDiagnosticsDialog',
            TECH_SUPPORT_DIALOG_CHOOSE_TECHNOLOGY: '#techDiagnChooseTechnologyDialog',
            TECH_TITLE: '.onPagePopup__body .techDiagnTitle',
            TECH_PROGRESS_BAR: '#statusbarInProgress',
            TECH_PROGRESS_BAR_CONTAINER: '.statusbar',
            LIST_OF_CASES: '.onPagePopup_verificationForm__aside .listOfCases',
            LIST_OF_RESULTS: '.onPagePopup_verificationForm__optionsList',
            BUTT_TECH_SUPPORT: '.actionTechSupport',
            BUTT_TECH_CONTINUE: '.actionTechContinue',
            CHOICE_OPTION_BTN: '.choised_option__btn',
            NEGATIVE_BTN: '.negative__btn',
            BUTTON_CHECKED_OPTION: '.buttonCheckedOption',
            TECH_DIAGNOSTICS_DIALOG: "div[id='techDiagnosticsDialog']",
            TECH_ASIDE_ITEM: "div[class='onPagePopup_verificationForm__aside']",
            TECH_ID_BODY_CONTAINER: "div[id='techBodyContainer']",
            TECH_OPTIONS_LIST_ITEM: "ul[class='onPagePopup_verificationForm__optionsList']"
        },
        TEMPLATE: {
            NEXT_CASE: '<li><b>{messageNextCase}</b></li>',
            CHECKED_REQUEST: '<li><img src="/cockpit/images/loading_circle_small.gif" /><span style="margin-left: 5px;">{message}</span></li>',
            CHECKED_RESULT: '<li><i class="statusChecked"></i>{message}</li>',
            DENY_RESULT: '<li><i class="statusDeny"></i>{message}</li>',
            CATEGORY_ITEM: '<li class="categotyType current">{message}</li>'
        },
        URLS: {
            CHECK_TYPE_USER_SERVICE_URL: '/check/type/user/service',
            CHECK_TSS_ACTIVE_URL: '/check/tss/active',
            TSS_URL: '/cockpit/tss',
            SAVE_CHECKED_OPTION: '/check/add/checked/option',
            LOAD_CHECKED_OPTION: '/check/get/checked/option'
        },
        PRM: {
        	PRM1: "PARAMETER01",
        	PRM2: "PARAMETER02",
        	PBAR: "PROGRESS"
        },
        DATA_SEARCH_ALL_RESULT_URL: 'search-all-result-url',
        REBOOT_TYPE: {
        	REBOOT_DEVICE_CONNECTED: 'docsis-device-connected',
        	REBOOT_UNSTABLE: 'docsis-unstable',
        	REBOOT_SPEED_TEST: 'docsis-speed-test',
        	RUN_ALIGN_PROFILE: 'docsis-align-profile',
        	RUN_ALIGN_PROFILE_ETHERNET: 'docsis-align-profile-ethernet'
        }
    };

    var Support = function() {};
    var doPositiveAction = function() {};
    var doNegativeAction = function() {};

    $.extend(Support.prototype, {

        init: function() {
            var _this = this;
            _this._messages = CP.Support.Messages.prototype;
            _this.timeOutStartFunction = 1500;
            _this.initTemplates();
            _this.progress = 0;
            _this.progressDetermin = 20;
            _this.bindEvents();
            
            _this.rebootDocsisDeviceConnected = DEFAULTS.REBOOT_TYPE.REBOOT_DEVICE_CONNECTED;
            _this.rebootDocsisUnstable = DEFAULTS.REBOOT_TYPE.REBOOT_UNSTABLE;
            _this.rebootDocsisSpeedTest = DEFAULTS.REBOOT_TYPE.REBOOT_SPEED_TEST;
            _this.runAlignProfile = DEFAULTS.REBOOT_TYPE.RUN_ALIGN_PROFILE;
            _this.runAlignProfileEthernet = DEFAULTS.REBOOT_TYPE.RUN_ALIGN_PROFILE_ETHERNET;
            
            _this._regionGeoName = $('.yourCity a').data('region-geo-name');

            $(CP.Support).on('cp.support.messages.added', function() {
                _this.initUpdateProgressBar();
            });
            // Загрузка данных с опозданием.
            _this.initLoadingDataDelayed();
        },


        bindEvents: function() {
            var _this = this;
            $(DEFAULTS.SELECTORS.BUTT_TECH_SUPPORT).off('click').on('click', function(event) {
                event.preventDefault();
                _this.initDialog();
            });
        },

        checkCalledTechDiagnostic: function() {
        	var _this = this;
            var isExistCall = $.cookie('call-support-type');
            isExistCall = isExistCall == null ? "" : isExistCall; 
            var isRebootSystem = $.cookie('call-reboot-system');
            isRebootSystem = isRebootSystem == null ? "" : isRebootSystem; 
            if (isExistCall.length > 0 && isRebootSystem.length > 0) {
            	if (_this.checkPrepaidContact()) {
            		this.handlFragmentAfterReboot(isRebootSystem);
            	}
                $.cookie('call-support-type', '', {
                    path: '/'
                });
                $.cookie('call-reboot-system', '', {
                    path: '/'
                });
            } else if (isExistCall.length > 0) {
            	if (_this.checkPrepaidContact()) {
                	this.handlSupportType(isExistCall);
            	}
                $.cookie('call-support-type', '', {
                    path: '/'
                });
            } else if (isRebootSystem.length > 0) {
            	this.handlFragmentAfterReboot(isRebootSystem);
                $.cookie('call-reboot-system', '', {
                    path: '/'
                });
            }
        },

        checkPrepaidContact: function() {
            var result = false;
            var block = $("#checkPrepaidContact");
            if (block.length > 0) {
            	var actionUrl = $(block).attr("action-url");
                var dataObj = CP.Cockpit.Form.SubmitProvider.simpleCloning($(block).data());
                var jsonResponse = CP.Cockpit.Form.SubmitProvider.runAjaxPostNotAsyncRequest(actionUrl, dataObj);
                if (jsonResponse != null && jsonResponse.infoMessage != null && jsonResponse.infoMessage.length > 0) {
                	$.fancybox.hideLoading();
                	CP.Cockpit.Form.SubmitProvider.ajaxJsonResposeHandler(jsonResponse);
                } else {
                	result = true;
                }
            } else {
            	console.error("Error: No adjustment 'checkPrepaidContact' to check Prepaid.");
            }
            return result;
        },
        
        initTemplates: function() {

            var endTechDiagnosticTempl = dust.compile($("#endTechDiagnosticTempl").html(), 'endTechDiagnosticTempl');
            dust.loadSource(endTechDiagnosticTempl);

            var choiceOptionTempl = dust.compile($("#choiceOptionTempl").html(), 'choiceOptionTempl');
            dust.loadSource(choiceOptionTempl);
        },

        initDialog: function() {
            var _this = this;
            var notLoggedIn = $.cookie('Volia-Customer-LoggedIn');
            if (notLoggedIn) {
                var url = _this.createTargetUrl(DEFAULTS.URLS.CHECK_TSS_ACTIVE_URL);
                _this.doCallCheck(url).done(function(response) {
                    _this.handleTssSupport(response);
                });
            }
        },

        handleTssSupport: function (isTssAvailable) {
            var _this = this;
            if(!isTssAvailable){
                CP.Cockpit.showInfoDialogWithRedirectAfter(_this._messages.messageFunctionalityInProgress(), null);
            } else {
                window.location = $('#tssId').attr("action-url");
            }
        },

        handlSupportType: function(type) {
            var _this = this;
            if (_this.getLightVersionCheckerIsLight() // только для Light версии
            		&& (!_this.getLightVersionCheckerIsCityForShow() // нет в списке доступных городов
            				|| _this.getBillingAttributesIsTechnologiesOtt()) ) { // или присутствует OTT
            	CP.Cockpit.showInfoDialogWithRedirectAfter(_this._messages.messageFunctionalityInProgress(), null);
            } else {
                if (type === 'TV_INET') {
                    _this.initDialogChoiceService();
                } else if (type === 'TV') {
                    _this.initDialogTv();
                } else if (type === 'INET') {
                    _this.initDialogInternet();
                }
            }
        },
        
        getLightVersionCheckerIsCityForShow: function() {
        	var result = false;
        	var lightVersionCheckerIsCityForShow = $('input[name="lightVersionCheckerIsCityForShow"]').val();
        	if (lightVersionCheckerIsCityForShow != null) {
        		result = lightVersionCheckerIsCityForShow === "true";
        	}
        	return result;
    	},

        // Получить признак Light версии.
        getLightVersionCheckerIsLight: function() {
        	var result = false;
        	var lightVersionCheckerIsLight = $('input[name="lightVersionCheckerIsLight"]').val();
        	if (lightVersionCheckerIsLight != null) {
        		result = lightVersionCheckerIsLight === "true";
        	}
        	return result;
    	},
    	
    	// Получить признак наличия технологии OTT.
    	getBillingAttributesIsTechnologiesOtt: function() {
        	var result = false;
        	var element = $('div[id="billingAttrIsTechnologiesOtt"]');
        	if (element) {
        		result = element.attr('result-item') === "true";
        	}
        	return result;
    	},
    	
        handlFragmentAfterReboot: function(type) {
            var _this = this;
            // TODO[01]
            if (type === DEFAULTS.REBOOT_TYPE.REBOOT_DEVICE_CONNECTED) {
            	this.showDialog();
                CP.CheckDocsis.prototype.afterReboot2_4_2();
            } else if (type === DEFAULTS.REBOOT_TYPE.REBOOT_UNSTABLE) {
            	this.showDialog();
                CP.CheckDocsis.prototype.questionWorkOfServiceRestored2_6();
            } else if (type === DEFAULTS.REBOOT_TYPE.REBOOT_SPEED_TEST) {
            	this.showDialog();
                CP.CheckDocsis.prototype.runSpeedTestToCheck2_7();
            } else if (type === DEFAULTS.REBOOT_TYPE.RUN_ALIGN_PROFILE) {
             _this.initDialogCheckEquipment();
            	CP.CheckDocsis.prototype.checkOldDocsisVersion2_3();
            } else if (type === DEFAULTS.REBOOT_TYPE.RUN_ALIGN_PROFILE_ETHERNET) {
             _this.initDialogCheckEquipment();
           	CP.cockpit.support.CheckEthernet.prototype.serviceIsWork5_2();
           }
           //TODO This need implement other cases
        },
        
        initDialogCheckEquipment: function(){
        	var _this = this;
        	_this.showDialog();
          	_this.addNewCheckedCategory(_this._messages.messageCategoryCheckEquipment());
          	_this.setTitleTextInDialog(_this._messages.titleCheckStatusEquipment());
        },

        initDialogTv: function() {
            this.showDialog();
            CP.CheckServices.prototype.checkServiceStatus1();
        },

        initDialogInternet: function() {
            //TODO Rub internet check. It temporary
            this.showDialog();
            /*var Checker = new CP.Support.Checker('internet', this, CP.ns('CP.Site.Menu').lang);
            Checker.init();*/
            var CheckEthernet = new CP.cockpit.support.CheckEthernet();
            CheckEthernet.init();
        },

        initDialogChoiceService: function() {
            var _this = this;
            $('.choiceOptionCheckBox').each(function(index, item) {
                var $item = $(item);
                $item.prop('checked', false);
            });
            $('#checkBoxTvType').prop('checked', true);
            CP.Cockpit.showDialogInFancyBox(DEFAULTS.SELECTORS.TECH_SUPPORT_DIALOG_CHOOSE_TECHNOLOGY);
            $('.choiceOptionCheckBox').off().on('click', _this.doSwitchRadioCheckBoxes);
            $(DEFAULTS.SELECTORS.BUTT_TECH_CONTINUE).off().on('click', $.proxy(_this.doStartChoseChecked, this));
        },

        doStartChoseChecked: function() {
            var _this = this;
            if (_this.getNumberOfCheckedCheckBox() === 0) {
                _this.initDialogTv();
            } else {
                _this.initDialogInternet();
            }
        },

        initUpdateProgressBar: function() {
            var $progressBar = $(DEFAULTS.SELECTORS.TECH_PROGRESS_BAR);
            var _this = this;
            if (_this.progress < 100) {
                _this.progress = _this.progress + _this.progressDetermin;
            }
            _this.updateProgressBarAndCheckMaxValue();
        },
        
        updateProgressBarAndCheckMaxValue: function() {
            var _this = this;
            if ($(DEFAULTS.SELECTORS.TECH_PROGRESS_BAR)) {
                if (_this.progress == 100) {
                    $(DEFAULTS.SELECTORS.TECH_PROGRESS_BAR_CONTAINER).removeClass('statusbar_inprogress');
                }
                $(DEFAULTS.SELECTORS.TECH_PROGRESS_BAR).css({
                    'width': _this.progress + '%'
                });
            }
        },
        
        // Загрузка данных с опозданием.
        initLoadingDataDelayed: function() {
        	// Определить список элементов для получения доп.сведений от биллинга.
        	var billingAttributes = $('#blocksDashboardBillingAttributes');
        	if (billingAttributes) {
        		var elements = billingAttributes.find('div');
        		if (elements) {
            		var csrf = $("input[name=\"_csrf\"]").val();
            		elements.each(function(indx, element) {
            			var actionUrl = $(element).attr('action-url');
            	        $.ajax({
            	            url: actionUrl,
            	            cache: false,
            	            type: "POST",
            	            async: true,
            	            data: {_csrf: csrf}
            	        }).done(function (response) {
            	            if (response != null) {
            	            	$(element).attr('result-item', response);
            	            } 
            	        }).fail(function (data) {
            	        	console.error("Error retrieving \"" + $(element).attr('id') + "\" from the billing.");
            	        });
            		});
        		}
        	}
        },
        
        loadRequestData: function(message) {
        	this.checkExistResultContainer();
        	$(DEFAULTS.SELECTORS.LIST_OF_RESULTS).append(DEFAULTS.TEMPLATE.CHECKED_REQUEST.replace('{message}', message));
        	$(DEFAULTS.SELECTORS.TECH_BODY_CONTAINER2).empty();
        },

        loadResponseData: function(response) {
        	this.checkExistResultContainer();
        	$(DEFAULTS.SELECTORS.LIST_OF_RESULTS + ' li:last-child').remove();
            if (response.status) {
                $(DEFAULTS.SELECTORS.LIST_OF_RESULTS).append(DEFAULTS.TEMPLATE.CHECKED_RESULT.replace('{message}', response.message));
            } else {
                $(DEFAULTS.SELECTORS.LIST_OF_RESULTS).append(DEFAULTS.TEMPLATE.DENY_RESULT.replace('{message}', response.message));
            }
            $(CP.Support).trigger('cp.support.messages.added');
        },
        
        checkExistResultContainer: function(){
        	if (!$(DEFAULTS.SELECTORS.LIST_OF_RESULTS).length) {
                this.renderCheckServiceFragment();
            }
        },

        setTitleTextInDialog: function(title) {
            if ($(DEFAULTS.SELECTORS.TECH_TITLE).length) {
                $(DEFAULTS.SELECTORS.TECH_TITLE).text(title);
            }
        },

        addNewCheckedCategory: function(nameCategory) {
            $('.listOfCases .categotyType').each(function() {
                $(this).removeClass("current");
            });
            $(DEFAULTS.SELECTORS.LIST_OF_CASES).append(DEFAULTS.TEMPLATE.CATEGORY_ITEM.replace('{message}', nameCategory));
        },

        renderCheckServiceFragment: function(response) {
            var checkedServicesTempl = dust.compile($("#checkedServicesTempl").html(), 'checkedServicesTempl');
            dust.loadSource(checkedServicesTempl);

            this.renderAutoCheckTemplFragment(response, 'checkedServicesTempl');
        },
        
        renderAutoCheckTemplFragment: function(response, nameTempl) {
            dust.render(nameTempl, response, function(err, res) {
                $(DEFAULTS.SELECTORS.TECH_BODY_CONTAINER).html(res);
            });
            $(CP.Support).trigger('cp.support.fragment.rendered');
        },

        renderEndTechDiagnFragment: function(response) {
            this.renderTemplFragment(response, 'endTechDiagnosticTempl');
        },

        renderPayBalanceFragment: function(response) {
            var techDiagnosticPayTempl = dust.compile($("#techDiagnosticPayTempl").html(), 'techDiagnosticPayTempl');
            dust.loadSource(techDiagnosticPayTempl);

            this.renderTemplFragment(response, 'techDiagnosticPayTempl');
            this.bindEventToOptionButton();
        },

        renderChoiceOptionFragment: function(response) {
            this.renderTemplFragment(response, 'choiceOptionTempl');
            $(DEFAULTS.SELECTORS.CHOICE_OPTION_BTN).on('click', $.proxy(this.actionChoiceOption, this));
            $('.choiceOptionCheckBox').on('click', this.doSwitchRadioCheckBoxes);
        },

        renderTemplFragment: function(response, nameTempl) {
            dust.render(nameTempl, response, function(err, res) {
                $(DEFAULTS.SELECTORS.TECH_BODY_CONTAINER2).html(res);
            });
            $(CP.Support).trigger('cp.support.fragment.rendered');
        },

        renderStaticFragment: function(targetUrl) {
            $.ajax({
                url: targetUrl,
                cache: false,
                success: function(html) {
                    $(DEFAULTS.SELECTORS.TECH_BODY_CONTAINER2).html(html);
                    $(CP.Support).trigger('cp.support.fragment.rendered');
                },
                error: function(e) {
                    console.error(e);
                }
            });
        },

        showDialog: function() {
            var _this = this;
            CP.Cockpit.closeDialogInFancyBox();
            var isRebootSystem = $.cookie('call-reboot-system');
            _this.renderCheckServiceFragment();
            $('.categotyType').each(function() {
                $(this).remove();
            });
            $(DEFAULTS.SELECTORS.LIST_OF_RESULTS).empty();
            _this.progress = 0;
            if (isRebootSystem != null && isRebootSystem.length) { // Восстановить список результатов выполненных проверок
            	_this.restoreResultsTestsPerformed();
            }
            _this.updateProgressBarAndCheckMaxValue();
            //CP.Cockpit.showDialogInFancyBox(DEFAULTS.SELECTORS.TECH_SUPPORT_DIALOG);
            // Требуется пауза, что бы закрылось предыдущее окно FancyBox.
            // Иначе скроллинг может скрыть новое окно.
            window.setTimeout(function() {
            	CP.Cockpit.showDialogInFancyBox(DEFAULTS.SELECTORS.TECH_SUPPORT_DIALOG);
            }, 600);
        },

        //For create json array of check box options. Need to pass array of option names in right order
        cteateOptionsForChoise: function(optionsArray) {
            jsonObj = [];

            $.each(optionsArray, function(index, value) {

                var item = {};
                item["index"] = index;
                item["value"] = value;

                jsonObj.push(item);
            });

            return jsonObj;
        },

        doSwitchRadioCheckBoxes: function(e) {
            var $this = $(e.target);
            var $checkBoxes = $('.choiceOptionCheckBox');

            if ($checkBoxes.length) {
                $checkBoxes.each(function(index, item) {
                    var $item = $(item);
                    $item.prop('checked', false);
                });
            }
            $this.prop('checked', true);
            $(DEFAULTS.SELECTORS.CHOICE_OPTION_BTN).removeAttr('disabled');
        },

        actionChoiceOptionAskPausedService: function(event) {
            event.preventDefault();
            var index = Support.prototype.getNumberOfCheckedCheckBox();
            if (index == 0) {
                var callUrl = Support.prototype.createTargetUrl(event.data.url);
                CP.Cockpit.Form.SubmitProvider.runAjaxPostRequest(callUrl, {
                    _csrf: $('input[name="_csrf"]').val()
                });
            } else {
                CP.Cockpit.closeDialogInFancyBox();
            }

        },

        getNumberOfCheckedCheckBox: function() {
            //var $this = $(e.target);
            var index = 0;
            $('.choiceOptionCheckBox').each(function() {
                var checked = $(this).prop('checked');
                if (checked) {
                    index = $(this).data("index");
                }

            });
            return index;
        },

        createTargetUrl: function(actionUrl) {
            var baseUrl = $('input[name="baseTechUrl"]').val();
            return baseUrl + actionUrl;
        },

        doCallCheck: function(url) {
            var _this = this;
            return $.ajax({
                cache: false,
                method: 'POST',
                url: url,
                data: {
                    _csrf: $('input[name="_csrf"]').val()
                }
            }).fail(function() {
            	CP.Cockpit.showInfoDialogWithRedirectAfter(_this._messages.messageCouldNotGetData(), null);
            });
        },

        saveCheckedOption: function(name, value) {
            var url = this.createTargetUrl(DEFAULTS.URLS.SAVE_CHECKED_OPTION);
            $.ajax({
                cache: false,
                method: 'POST',
                url: url,
                data: {
                    _csrf: $('input[name="_csrf"]').val(),
                    optionName: name,
                    optionValue: value
                },
                error: function(e) {
                    console.error(e);
                }
            });
        },

        bindEventToOptionButton: function() {
            $(DEFAULTS.SELECTORS.BUTTON_CHECKED_OPTION).on('click', function() {
                var optionName = $(this).data('option-name');
                var optionValue = $(this).data('option-value');
                CP.Support.prototype.saveCheckedOption(optionName, optionValue);
            });
            $('#accountPaymentBillId').on('keyup', function(event) {
                var $this = $(this);
                var filterVal = $.trim($this.val());
                if (filterVal.length >= 9) {
                    CP.Cockpit.sendContractCreditCode(event);
                }
            });
        },
        
        requestServerSynchronous: function(url, data) {
        	var _this = this;
            var result = null;
            $.ajax({
                cache: false,
                method: 'POST',
                async: false,
                url: url,
                data: data,
                error: function(e) {
                    console.error("requestServerSynchronous(url='" + url + "') " + e);
                },
                success: function (data, textStatus) {
                	result = data;
                }
            });
            return result;
        },
        
        putCheckedParameter: function(paramName, paramValue) {
        	var _this = this;
            var url = this.createTargetUrl("/check/put/parameter"); // TODO temp; DEFAULTS.URLS.SAVE_CHECKED_OPTION);
            var data = { _csrf: $('input[name="_csrf"]').val(), parameterName: paramName, parameterValue: paramValue };
            return _this.requestServerSynchronous(url, data);
        },

        getCheckedParameter: function(paramName) {
        	var _this = this;
            var url = this.createTargetUrl("/check/get/parameter"); // TODO temp; DEFAULTS.URLS.SAVE_CHECKED_OPTION);
            var data = { _csrf: $('input[name="_csrf"]').val(), parameterName: paramName };
            return _this.requestServerSynchronous(url, data);
        },
        
        cleanCheckedParameter: function(paramName) {
        	var _this = this;
            var url = this.createTargetUrl("/check/clean/parameter"); // TODO temp; DEFAULTS.URLS.SAVE_CHECKED_OPTION);
            var data = { _csrf: $('input[name="_csrf"]').val() };
            return _this.requestServerSynchronous(url, data);
        },

        getElement: function(element, select) {
        	var result = null;
        	if (element != null && element.length > 0 && select != null && select.length > 0) {
        		result = $(element).find(select);
        	}
        	return result;
        },
        
        // Сохранить список результатов выполненых проверок
        saveResultsTestsPerformed: function() {
        	var _this = this;
        	var techDiagnosticsDialog = $(DEFAULTS.SELECTORS.TECH_DIAGNOSTICS_DIALOG)
        	// Получить список состояний "тех.диагностика"
        	var asideItems = CP.Support.prototype.getElement(techDiagnosticsDialog, DEFAULTS.SELECTORS.TECH_ASIDE_ITEM);
        	if (asideItems != null && asideItems.length > 0) {
        		var parameter1 = $(asideItems).html();
        		// Сохранить список как параметер
        		CP.Support.prototype.putCheckedParameter(DEFAULTS.PRM.PRM1, parameter1);
        	}
        	// Получить список выполненых проверок
        	var techBodyContainer = CP.Support.prototype.getElement(techDiagnosticsDialog, DEFAULTS.SELECTORS.TECH_ID_BODY_CONTAINER);
        	var optionItems = CP.Support.prototype.getElement(techBodyContainer, DEFAULTS.SELECTORS.TECH_OPTIONS_LIST_ITEM);
        	if (optionItems != null && optionItems.length > 0) {
        		var parameter2 = $(optionItems).html();
        		// Сохранить список как параметер
        		CP.Support.prototype.putCheckedParameter(DEFAULTS.PRM.PRM2, parameter2);
        	}
        	var progress = "" + _this.progress;
    		// Сохранить значение progress выполненных проверок
    		CP.Support.prototype.putCheckedParameter(DEFAULTS.PRM.PBAR, progress);
        },

        // Восстановить список результатов выполненных проверок
        restoreResultsTestsPerformed: function() {
        	var _this = this;
        	var techDiagnosticsDialog = $(DEFAULTS.SELECTORS.TECH_DIAGNOSTICS_DIALOG)
        	// Обновить список состояний "тех.диагностика"
        	var asideItems = CP.Support.prototype.getElement(techDiagnosticsDialog, DEFAULTS.SELECTORS.TECH_ASIDE_ITEM);
        	if (asideItems != null) {
        		var parameter1 = CP.Support.prototype.getCheckedParameter(DEFAULTS.PRM.PRM1);
        		if (parameter1 != null && parameter1.length > 0) {
            		$(asideItems).html(parameter1);
        		}
        	}
        	// Обновить список выполненых проверок
        	var techBodyContainer = _this.getElement(techDiagnosticsDialog, DEFAULTS.SELECTORS.TECH_ID_BODY_CONTAINER);
        	var optionItems = _this.getElement(techBodyContainer, DEFAULTS.SELECTORS.TECH_OPTIONS_LIST_ITEM);
        	if (optionItems != null) {
        		// Получить список как параметр
        		var parameter2 = CP.Support.prototype.getCheckedParameter(DEFAULTS.PRM.PRM2);
        		if (parameter2 != null && parameter2.length > 0) {
            		$(optionItems).html(parameter2);
        		}
        	}
    		// Получить значение progress выполненных проверок
    		var progress = -1;
    		var progressStr = CP.Support.prototype.getCheckedParameter(DEFAULTS.PRM.PBAR);
    		if (progressStr != null && progressStr.length > 0) {
    			progress = Number(progressStr);
        		if (progress != null && -1 < progress && progress < 101) {
        			_this.progress = progress;	
        		}
    		}
    		CP.Support.prototype.cleanCheckedParameter();
        },
    });

    CP.register(Support.prototype);

    return Support;

})(window.jQuery, window._, CP);