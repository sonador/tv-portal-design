CP.ns('CP.Site.CompareChannels');

CP.Site.CompareChannels = (function($, CP, _) {

	var DEFAULTS = {
		SECTION_CONTAINER: '.channelsCompare',
		CHANNEL_CONTAINER: '.channelsCompare__channelsList',
		FILTER_BY_PACKAGE: '.channels__filter__by__package',
		FILTER_BY_CATEGORY: '.channels__filter__by__category',
		PACKAGE_TAB: '.package__tab',
		CHANNEL_FANCYBOX: '.btnFancybox',
		CATEGORY_CONTAINER: '#categories',

		FORM_ID: '#searchChannel'
	};

	var CompareChannels = function() {
	};

	$.extend(CompareChannels.prototype, {
		init: function() {
			var categoriesChannelTemplate = $('#categories-channel-template');
			var categoriesTemplate = $('#categories-template');
			var notFindChannel = $('#channels-not-found-template');
			this.$ajaxLoader = $('#ajaxLoader');

			if (!categoriesChannelTemplate.length && !categoriesTemplate.length) {
				// not found template
				return;
			}

			this.$container = $(DEFAULTS.SECTION_CONTAINER);
			var channels = dust.compile(categoriesChannelTemplate.html(), 'channels');
			var categories = dust.compile(categoriesTemplate.html(), 'categories');
			var notFindTemplate = dust.compile(notFindChannel.html(), 'not-find-channel');
			this.filter = {cats: [], pkgs: []};
			this.contentUrl = this.$container.data('content-url');

			this.loadPageContent();
			dust.loadSource(channels);
			dust.loadSource(categories);
			dust.loadSource(notFindTemplate);
		},

		loadPageContent: function() {
			var _this = this;
			$.ajax({
				url: _this.contentUrl,
				dataType: 'html',
				beforeSend: function () {
					_this.$container.html(_this.$ajaxLoader.find('img').clone().css({'margin-top' : '110px', 'margin-left':'300px'}));
                },
			}).done(function(content) {
				_this.$container.html(content);
				_this.bindEvents();
				_this.$form = $(DEFAULTS.FORM_ID);
				_this.checkSelected();
				_this.$checkedCategories = $('input[name=cc]:checked');
				_this.$checkedPackages = $('input[name=pckg]:checked');
				_this.appendUrlHashes();
				_this.filterByCategory();
				_this.filterByPackages();
				_this.resolvePackageUrls();
			}).error(function() {
				console.error("Error occurred");
			});
		},

		bindEvents: function bindEvents() {
			var _this = this;
			_this.$container.off('submit').on('submit', DEFAULTS.FORM_ID, $.proxy(_this.searchChannels, _this));
			_this.$container.on('click', DEFAULTS.FILTER_BY_CATEGORY, _this, _this.hideOrDisplayBlockCategories);
			_this.$container.on('click', DEFAULTS.FILTER_BY_PACKAGE, _this, _this.hideOrDisplayBlockPackageAndChannels);

			_this.$container.find(DEFAULTS.CHANNEL_FANCYBOX).fancybox({
				padding: 0,
				autoResize: false,
				width: 930,
				scrolling: 'no',
				fitToView: false,
				closeBtn: false,
				beforeShow: function() {
					$('.fancybox-inner .scrollbar-inner').scrollbar();
				}
			});

		},

		checkSelected: function() {
			var hash = window.location.hash.replace('#', '');
			var values = hash.split('&');
			if (values && values.length) {
				_.each(values, function(val) {
					$('input[data-filter-index=' + val + ']').prop('checked', true);
				});
			}
		},

		appendUrlHashes: function() {
			var _this = this;
			_this.$checkedPackages.each(function() {
				var pkgFilterIndex = $(this).data('filter-index');
				if (!_.contains(window.location.hash, pkgFilterIndex)) {
					_this.appendHash(pkgFilterIndex);
				}
			});
			_this.$checkedCategories.each(function() {
				var catFilterIndex = $(this).data('filter-index');
				if (!_.contains(window.location.hash, catFilterIndex)) {
					_this.appendHash(catFilterIndex);
				}
			});
		},

		searchChannels: function(e) {
			e.preventDefault();
			var _this = this;
			$("input[type=submit]").attr("disabled", "disabled");
			try {
				var redirect = _this.$form.data("base-url");
				var actionUrl = _this.$form.data("action-url");
				var prmExtPackageId = _this.$form.data("ext-package-id");
				var prmType = _this.$form.data("type");
				var prmQueryName = this.$form.find("input[name=\"search\"]").val();
				if (prmQueryName != null && prmQueryName.length > 0) {
					prmQueryName = encodeURIComponent(prmQueryName);
				} 
				var csrfValue = $("input[name=\"_csrf\"]").val();
				if (prmQueryName.length > 0) {
					$.ajax({
						url: actionUrl,
	                    type: "GET",
	                    data: {
	                    	exPackageId: prmExtPackageId,
	                    	type: prmType,
	                    	query: prmQueryName,
	                    	_csrf: csrfValue
	                    },
						success: function(data, textStatus, jqXHR) {
	                        if (textStatus != "success") {
	                            location.href = redirect;
	                        } else {
	                        	var fragment = $(".fragment-channels-category");
	                        	if (data != null && data.length > 0 && fragment != null) {
	        						$("#channelsCompareListId").remove();
	        						$(fragment).append(data);
	        						_this.filterByPackages();
	                        	}
	                        }
						}
					});
				} else {
					location.href = redirect;
				}
			} finally {
				$("input[type=submit]").removeAttr("disabled");
			}
		},

		hideOrDisplayBlockCategories: function(e) {
			var _this = e.data;
			_this.appendHash($(this).data('filter-index'));
			_this.filterByCategory();
		},

		filterByCategory: function() {
			var checkedCatIds = [];
			$('input[name=cc]:checked').each(function() {
				var $el = $(this);
				var categoryId = $el.data('category-id');
				checkedCatIds.push('#' + categoryId);
			});

			var $listSection = $('.channelsCompare__channelsList__section');
			if (checkedCatIds.length) {
				var idsSelector = checkedCatIds.join(',');
				$listSection.removeClass('show');
				$(idsSelector).addClass('show').show();
				$listSection.not('.show').hide();
			} else {
				$listSection.addClass('show').show();
			}
		},

		hideOrDisplayBlockPackageAndChannels: function(e) {
			var _this = e.data;
			_this.appendHash($(this).data('filter-index'));
			_this.filterByPackages();
		},

		filterByPackages: function() {
			var _this = this;
			var chnItems = $("a[data-pckg-id-list]");
			if (chnItems == null || chnItems.length == 0) {
				return
			}
			
			var pcItems = $("input[name=pckg]:checked");  // список отмеченных доп.пакетов.
			var isNotEmptyPcItems = pcItems != null && pcItems.length > 0; 
			var pckgList = [];
			for (var i = 0; isNotEmptyPcItems && i < pcItems.length; i++) {
				pckgList[pckgList.length] = "," + $(pcItems[i]).attr("data-package-id") + ",";
			}
			var activeChannels = [];  // список количества активных каналов по категориям.
			var itemCompare = $("#channelsCompareListId");
			// Определить стиль активного элемента.
			var styleActive = itemCompare != null ? $(itemCompare).attr("data-style-active") : "";
			styleActive = styleActive == null ? "" : styleActive;
			// Определить стиль пассивного элемента.
			var stylePassive = itemCompare != null ? $(itemCompare).attr("data-style-passive") : "";
			stylePassive = stylePassive == null ? "" : stylePassive;
			
   			if (pckgList.length > 0) {				// если есть отмеченные доп.пакеты, то показывать каналы 
				var chnItemsCnt = chnItems.length;	// для них и если data-basic-package="true".
				for (var i = 0; i < chnItemsCnt; i++) {
					var chnItem = chnItems[i];
					if (chnItem == null) {
						continue;
					}
					var isShow = false;
					var strPackageIdList = $(chnItem).attr("data-pckg-id-list");
					if (strPackageIdList != null) {
						for (var j = 0; j < pckgList.length; j++) {
							if (isShow = (("," + strPackageIdList + ",").indexOf(pckgList[j]) != -1)) {
								break;
							}
						}
					}
					if (false === isShow) {
						isShow = ("true" === $(chnItem).attr("data-basic-package"));
					}
					$(chnItem).attr("style", (true == isShow ? styleActive : stylePassive));
						
					var code = $(chnItem).attr("data-category-code")
					if (activeChannels[code] == null) {
						activeChannels[code] = 0;
					}
					activeChannels[code] = activeChannels[code] + (true == isShow ? 1 : 0);
				}
				for (var i = 0; i < activeChannels.length; i++) {
					var categoryLable = $("span[data-category-code=" + i + "]");
					if (categoryLable != null && categoryLable.length > 0) {
						categoryLable.text(activeChannels[i]);
					}
				}
			}
			else { // если нет отмеченных доп.пакетов, то отобразить начальное кол-во активных каналов из атрибута.
   				var categoryLableSet = $("span[data-category-code]");
   				if (categoryLableSet != null && categoryLableSet.length > 0) {
   					for (var i = 0; i < categoryLableSet.length; i++) {
   						var cnt = $(categoryLableSet[i]).attr("data-count-active-channels");
   						$(categoryLableSet[i]).text(cnt);
   					}
   				}// Для всех каналов отобразить начальный признак активности.
				var chnItemsCnt = chnItems.length;
				for (var i = 0; i < chnItemsCnt; i++) {
					var chnItem = chnItems[i];
					var isActive = $(chnItem).attr("data-is-active");
					$(chnItem).attr("style", isActive == "true" ? styleActive : stylePassive);
				}	
   			}
		},
		
		appendHash: function(val) {
			var hash = window.location.hash;
			var hashValues = hash.replace("#", '');
			var newValue = '&' + val;
			if (_.contains(hashValues, newValue)) {
				window.location.hash = hashValues.replace(newValue, '');
			} else {
				window.location.hash = hashValues + newValue;
			}
			this.resolvePackageUrls();
		},

		resolvePackageUrls: function() {
			$(DEFAULTS.PACKAGE_TAB).each(function() {
				var $el = $(this);
				var href = $el.attr('href');
				var regex = new RegExp("[#].{1,}", "g");
				href = href.replace(regex, '');
				var newHref = href + window.location.hash;
				$el.attr('href', newHref);
			});
			
			//For fix lang url in case to change category
			var newHrefForLang = $('.active .package__tab').attr('href');
		  	var currentLang = CP.Site.Menu.lang.toLowerCase();
	    	if (currentLang === 'uk'){
	    		newHrefForLang = newHrefForLang.replace('/uk', '/ru');
	    		$('#ruActionUrl').attr('href', newHrefForLang);
	    	} else {
	    		newHrefForLang = newHrefForLang.replace('/ru', '/uk');
	    		$('#ukActionUrl').attr('href', newHrefForLang);
	    	}
		}

	});

	CompareChannels.deactivateContentTabSelector = function() {
		$('.contentTabSelector').find('a').off('click');
	};

	CP.register(CompareChannels.prototype);

	return CompareChannels;

})
(jQuery, CP, window._);